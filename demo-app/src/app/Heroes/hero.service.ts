import {Hero} from "./hero";

export class HeroService{
 hero:Hero[]=[
    {name:"StrongOne",id:1},
    {name:"FearLessOne",id:2},
    {name:"AglieLizard",id:3},
    {name:"NaiveApple",id:4},
    {name:"SoftwareEngineer",id:5}

  ];

}
