import { Component } from '@angular/core';
import {Hero} from "./hero";
import {HeroService} from "./hero.service";

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent{
  constructor(heroService:HeroService) {
  this.heroes = heroService.hero;

  }
  selectedHero: Hero;
  heroes:Hero[];
  title:string="Heroes List";
  onSelect(hero: Hero) {

      this.selectedHero = hero;

  }

}
